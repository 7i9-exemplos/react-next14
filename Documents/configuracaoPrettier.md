# Configuração do Prettier
## A documentação pode ser encontrada em 


Você pode encontrar a configuração no site do [Prettier](https://prettier.io/docs/en/install)
## Instalação

1. npm install prettier eslint-config-prettier -D
2. npm install eslint-plugin-prettier -D



**Configurando**

1. Crie o arquivo prettierrc

{
    "trailingComma":"none",
    "semi":true,
    "singleQuote":true
}

trailingComma: é a virgula no ultimo indice do objeto -> none quer dizer que não dará erro
semi é a virgula no final de cada instrução
singleQuote é as aspas simples ''

2. criar um arquivo colocando vscode/settings.json

{
    "editor.formatOnPaste": false,
    "editor.codeActionsOnSave": {
        "source.fixAll": true
    },
 "typescript.tsdk": "./node_modules/typescript/lib",   
 "typescript.enablePromptUseWorkspaceTsdk": true
}
