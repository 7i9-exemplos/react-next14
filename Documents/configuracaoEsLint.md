# Configuração do ESLint


**Após a instalação do projeto**

1. instale as extensões Eslint e prettier
2. npm init @eslint/config
3. How would you like to use ESLint? · problems
4. What type of modules does your project use? · esm
5. Which framework does your project use? · react
6. Does your project use TypeScript? · No / Yes
7. Where does your code run? · browser, node
8. What format do you want your config file to be in? · JSON

## A documentação pode ser encontrada em 

[eslint-plugin-react-hooks](https://www.npmjs.com/package/eslint-plugin-react-hooks)

## As regras

"rules": {
        "react-hooks/rules-of-hooks": "error",
        "react-hooks/exhaustive-deps": "warn",
        "react/prop-types": "off",
        "react/react-in-jsx-scope":"off",
        "eslint-disable prettier/prettier":"true"
    }
## As extensões
 "extends": [
        "eslint:recommended",
        "plugin:@typescript-eslint/recommended",
        "plugin:react/recommended",
        "plugin:react-hooks/recommended",
        "plugin:prettier/recommended"
    ],

**Obs.: essa ultima linha vai mostrar os erros e ao salvar corrigir ex: inserir ; ou remover espaços**    