# Criação de testes Unitarios
**Passo a passo para formatação do teste unitario**

## Utilizando Jest

A documentação do Jest pode ser encontrada [Aqui](https://jestjs.io/pt-BR/docs/getting-started)

**Após a instalação do Jest deve ser instalado o test library**

A documentação pode ser encontrada em [testing-library/react](https://www.npmjs.com/package/@testing-library/react)


use o comando para instalação 
**npm install -D jest jest-environment-jsdom @testing-library/react @testing-library/jest-dom**