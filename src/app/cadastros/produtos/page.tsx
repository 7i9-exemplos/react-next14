import { deleteUser } from "@/app/lib/acao";
import { fetchUsers } from "@/app/lib/data";
import Pagination from "@/app/ui/dashboard/paginacao/paginacao";
import Search from "@/app/ui/dashboard/pesquisa/pesquisa";
import styles from "@/app/ui/dashboard/paginasDeCadastro/estiloCadastro.module.css";
import Image from "next/image";
import Link from "next/link";

const CadastroDeProdutos = async ({ searchParams }:any) => {
     
  const q = searchParams?.q || "";
  const page = searchParams?.page || 1;
  const { count, users }:any = await fetchUsers(q);

  return (
    <div className={styles.container}>
      <div className={styles.top}>
        <Search placeholder="Pesquisar produto..." />
        <Link href="/cadastros/produtos/produto">

          <button className={styles.addButton}>Novo</button>
        </Link>
      </div>
      <table className={styles.table}>
        <thead>
          <tr>
            <td>Name</td>
            <td>Email</td>
            <td>Created At</td>
            <td>Role</td>
            <td>Status</td>
            <td>Action</td>
          </tr>
        </thead>
        <tbody>
          {users?.map((user:any) => (
            <tr key={user.id}>
              <td>
                <div className={styles.user}>
                  <Image
                    src={user.img || "/noavatar.png"}
                    alt=""
                    width={40}
                    height={40}
                    className={styles.userImage}
                  />
                  {user.username}
                </div>
              </td>
              <td>{user.email}</td>
              <td>{user.createdAt?.toString().slice(4, 16)}</td>
              <td>{user.isAdmin ? "Admin" : "Client"}</td>
              <td>{user.isActive ? "active" : "passive"}</td>
              <td>
                <div className={styles.buttons}>
                  <Link href={`/dashboard/users/${user.id}`}>
                    <button className={`${styles.button} ${styles.view}`}>
                      View
                    </button>
                  </Link>
                  <form action={deleteUser}>
                    <input type="hidden" name="id" value={(user.id)} />
                    <button className={`${styles.button} ${styles.delete}`}>
                      Delete
                    </button>
                  </form>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
      <Pagination count={count} />
    </div>
  );
};

export default CadastroDeProdutos;