import { cards } from "../lib/data";
import Card from "../ui/dashboard/card/card";
import Chart from "../ui/chart/chart";
import styles from "../ui/dashboard/dashboard.module.css";
import Rightbar from "../ui/rightbar/right";
import Transactions from "../ui/transactions/transactions";

const Dashboard = () => {
  return (
    <div className={styles.wrapper}>
      <div className={styles.main}>
        <div className={styles.cards}>
          {cards.map((item) => (
            <Card item={item} key={item.id} />
          ))}
        </div>
        <Transactions />
        <Chart />
      </div>
      <div className={styles.side}>
        <Rightbar />
      </div>
    </div>
  );
};

export default Dashboard;