import Image from 'next/image'
import styles from './transactions.module.css'
const Transaction = () =>{
    return (
        <div className={styles.container}>
            <h2 className={styles.titulo}>Tabela de pagamentos</h2>
            <table className={styles.tabela}>
                <thead>
                    <tr>
                        <td>Nome</td>
                        <td>Status</td>
                        <td>Data</td>
                        <td>Valor</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <div className={styles.usuario}>
                            <Image src="user.svg" alt="" width={40} height={40} className={styles.usuarioImagem} />
                            John Doe
                            </div>
                        </td>
                        <td><span className={`${styles.status} ${styles.Aguardando}`}>Aguardando</span></td>
                        <td>14/02/2023</td>
                        <td>R$ 300,98</td>
                    </tr>
                    <tr>
                        <td>
                        <div className={styles.usuario}>
                            <Image src="user.svg" alt="" width={40} height={40} className={styles.usuarioImagem} />
                            John Doe
                            </div>
                        </td>
                        <td><span className={`${styles.status} ${styles.Cancelado}`}>Cancelado</span></td>
                        <td>14/02/2023</td>
                        <td>R$ 300,98</td>
                    </tr>
                    <tr>
                        <td>
                        <div className={styles.usuario}>
                            <Image src="user.svg" alt="" width={40} height={40} className={styles.usuarioImagem} />
                            John Doe
                            </div>
                        </td>
                        <td><span className={`${styles.status} ${styles.Pago}`}>Pago</span></td>
                        <td>14/02/2023</td>
                        <td>R$ 300,98</td>
                    </tr>
                </tbody>
            </table>
        </div>
    )
}
export default Transaction