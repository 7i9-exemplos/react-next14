import Image from 'next/image';
import MenuLink from './menuLink/menuLink';
import styles from './sidebar.module.css'
import { BiAlignJustify, BiLogOut } from "react-icons/bi";

 const Sidebar= ()=>{
    const MenuItems = [
        {
            titulo:'Pagina inicial',
            list:[
                {
                    titulo:'Dashboard',
                    path:'/dashboard',
                     icon:<BiAlignJustify/>
                },
            ]
        },
        {
            titulo:'Paginas',
            list:[
               
                {
                    titulo:'Produtos',
                    path:'/cadastros/produtos',
                    icon:<BiAlignJustify/>
                },
                {
                    titulo:'Usuarios',
                    path:'/cadastros/usuarios',
                    icon:<BiAlignJustify/>
                }

            ]
        },
        {
            titulo:'Relatórios',
            list:[
                {
                    titulo:'Receitas',
                    path:'/receitas',
                     icon:<BiAlignJustify/>
                },
            ]
        }
    ]
    return (
        <div className={styles.container}>
            <div className={styles.user}>
                <Image className={styles.userImage} src="../user.svg" alt="" width="50" height="50"/>
                <div className={styles.userDetail}>
                    <span className={styles.userNome}>Jhon Joe</span>
                    <span className={styles.userTitulo}>Administrador</span>
                </div>
            </div>
            <ul className={styles.lista}>
          {MenuItems.map((cat:any)=>(
           <li key={cat.titulo}>
          
           <span className={styles.cat}>{cat.titulo}</span>
           
           {cat.list.map((item:any)=>
            <MenuLink item={item} key={item.titulo}/>
            )}
            </li>
          ))}
          </ul>
          <button className={styles.sair}>
            <BiLogOut />
            Sair
        </button>
        </div>
    )
}
export default Sidebar