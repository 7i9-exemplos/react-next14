"use client"
import Link from 'next/link'
import styles from './menuLink.module.css'
import { usePathname } from 'next/navigation'

const MenuLink = ({item}:any) =>{
    const pathNome=usePathname()   
    return (
        <Link href={item.path} className={`${styles.container} ${pathNome==item.path && styles.active}`} >
            {item.icon}
            {item.titulo}
        </Link>
    )
}
export default MenuLink