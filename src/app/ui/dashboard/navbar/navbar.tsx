"use client"
import styles from './navbar.module.css'
import { usePathname } from 'next/navigation'
import Search from '../pesquisa/pesquisa';
import { MdNotifications, MdOutlineChat, MdPublic } from 'react-icons/md';
 const NavsBar= ()=>{
    const pathNome=usePathname()   
    var pagina:any='Dashboard'   
    if(pathNome!='/cadastros')
    pagina=pathNome    
    return (
        <div className={styles.container}>
           <div className={styles.titulo}>{pagina.split("/").pop()}</div>
           <div className={styles.menu}>
           <Search placeholder="Pesquisar..." />
            <div className={styles.icons}>
            <MdOutlineChat size={20} />
          <MdNotifications size={20} />
          <MdPublic size={20} />
            </div>
           </div>
        </div>
    )
}
export default NavsBar