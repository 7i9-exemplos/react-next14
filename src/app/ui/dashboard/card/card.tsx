import { BiUser } from 'react-icons/bi'
import styles from './card.module.css'
const Card = (props:any) =>{
    return (
        <div className={styles.container}>
            <BiUser size={24}/>
            <div className={styles.texts}>
                <span className={styles.titulo}>Total de usuarios</span>
                <span className={styles.numero}>10.273</span>
                <span className={styles.detalhe}><span className={styles.positivo}>12%</span> more than previous week</span>
            </div>

        </div>
    )
}
export default Card